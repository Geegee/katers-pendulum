# list of helper classes
from vpython import cylinder, textures, color, vector, arrow
import numpy as np

class PhysPendulum:
    def __init__(self, phi, dphi, parts, rot_axis, origin, dt):
        self.phi = phi
        self.dphi = dphi
        self.parts = parts
        self.origin = origin
        self.rot_axis = rot_axis
        self.rotate(phi)
        self.calc_comb_com()
        self.calc_total_moi()
        self.ang_mom = self.moi * self.dphi / dt

    def rotate(self, angle):
        for part in self.parts:
            part.rotate(angle=angle, axis=self.rot_axis, origin=self.origin)
            part.calc_com()

    def calc_comb_com(self):
        com = vector(0, 0, 0)
        total_mass = 0
        for part in self.parts:
            com += part.mass * part.com
            total_mass += part.mass
        com /= total_mass
        self.total_mass = total_mass
        self.com = com
        return 

    def calc_total_moi(self):
        total_moi = 0
        for part in self.parts:
            total_moi += part.moi + part.mass * (self.origin - part.com).mag2
        self.moi = total_moi
        return

class PhysCylinder(cylinder):
    def __init__(self, density, pos, axis, radius, color=color.black, texture=textures.metal):
        cylinder.__init__(self, pos=pos, axis=axis, radius=radius, color=color, texture=texture)
        self.density = density
        self.axis = axis
        self.mass = np.pi * self.density * self.radius ** 2 * self.axis.mag
        self.calc_com()
        self.calc_moi()

    def calc_moi(self):
        self.moi =  0.5 * self.mass * (self.radius ** 2 + self.axis.mag2 / 6)

    def calc_com(self):
        self.com = self.pos + 0.5 * self.axis

class CoordinateSystem:
    def __init__(self, pos=vector(0, 0, 0), length=2.0, colors=[color.red, color.green, color.blue]):
        self._x = arrow(pos=pos, axis=vector(length, 0, 0), color=colors[0])
        self._y = arrow(pos=pos, axis=vector(0, length, 0), color=colors[1])
        self._z = arrow(pos=pos, axis=vector(0, 0, length), color=colors[2])

        self._length = length
        self._pos = pos
        return 

    @property
    def pos(self):
        return self._pos

    @pos.setter
    def pos(self, new_pos):
        self._pos = new_pos
        self._x.pos = new_pos
        self._y.pos = new_pos
        self._z.pos = new_pos 
        return

    @property
    def length(self):
        return self._length

    @length.setter
    def length(self, new_length):
        self._length = new_length
        self._x.axis = vector(new_length, 0.0, 0.0)
        self._y.axis = vector(0.0, new_length, 0.0)
        self._z.axis = vector(0.0, 0.0, new_length)
        return 
