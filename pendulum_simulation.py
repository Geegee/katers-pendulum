from vpython import *
import numpy as np
import ruamel.yaml as yaml

with open('config.yaml') as f:
    katers_conf = yaml.safe_load(f)

gfactor = katers_conf['physics']['gfactor']


def evolve_pendulum(pendulum, dt):
    """
    function that calculates the next time step of the pendulum
    """
    angular_momentum = pendulum.total_mass * pendulum.com * gfactor * np.sin(pendulum.phi)
    pendulum.ang_mom += angular_momentum.z * dt
    dphi = pendulum.ang_mom * dt / pendulum.moi
    pendulum.phi += dphi
    pendulum.dphi = dphi
    return pendulum
