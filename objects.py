# file containing the objects of the katers pendulum
import numpy as np
import copy
from vpython import *
import ruamel.yaml as yaml

from classes import PhysPendulum, PhysCylinder

# get definitions from the config
with open('config.yaml') as f:
    katers_conf = yaml.safe_load(f)

# The Pendulum itself
rod_position = vector(0.0, 0.0, -katers_conf['geometry']['small_cyl_height'] / 2)
rod_axis = vector(0.0, 0.0, -katers_conf['geometry']['rod_height'])

small_cyl_position = vector(0.0, 0.0, katers_conf['geometry']['small_cyl_height'] / 2)
small_cyl_axis = vector(0.0, 0.0, -katers_conf['geometry']['small_cyl_height'])

big_cyl_position = vector(0.0, 0.0,
                          -katers_conf['geometry']['small_cyl_height'] / 2 - katers_conf['geometry']['rod_height']
                          + katers_conf['geometry']['big_cyl_height'])

big_cyl_position = vector(0.0, 0.0,
                          -katers_conf['geometry']['small_cyl_height'] / 2 - katers_conf['geometry']['rod_height'])

big_cyl_axis = vector(0.0, 0.0, -katers_conf['geometry']['big_cyl_height'])

rod = PhysCylinder(pos=rod_position, axis=rod_axis, radius=katers_conf['geometry']['rod_radius'], density=katers_conf['physics']['density'], color=color.black)
small_cyl = PhysCylinder(pos=small_cyl_position, axis=small_cyl_axis ,
                         radius=katers_conf['geometry']['small_cyl_radius'],
                         density=katers_conf['physics']['density'], color=color.black)
big_cyl = PhysCylinder(pos=big_cyl_position, axis=big_cyl_axis, radius=katers_conf['geometry']['big_cyl_radius'],
                       density=katers_conf['physics']['density'], color=color.black)
distance_to_floor = abs(-katers_conf['geometry']['small_cyl_height'] / 2 - katers_conf['geometry']['rod_height'] - 0.1)


# The floor 
floor_position = vector(0.0, 0.0,
                        -katers_conf['geometry']['small_cyl_height'] / 2
                        - katers_conf['geometry']['rod_height']
                        - katers_conf['geometry']['big_cyl_height'] - 0.1)
floor = box(pos=floor_position, size=vector(1.0, 1.0, 0.02), texture='ulm_logo_on_wood.png')
floor.rotate(angle=np.pi / 2, axis=vector(0, 0, 1.0))

# The Holder
holder_rod_position = vector(-0.5, 0, katers_conf['geometry']['rot_axis_z'])
holder_rod_length = 1.0
distance_to_floor = holder_rod_position.z - floor_position.z
opening_angle = 15 * np.pi / 180

stand_rod_length = distance_to_floor / np.cos(opening_angle)

stand_rod1_position = copy.copy(holder_rod_position)
stand_rod1_length = stand_rod_length

stand_rod2_position = copy.copy(holder_rod_position)
stand_rod2_length = stand_rod_length

stand_rod3_position = copy.copy(holder_rod_position)
stand_rod3_position.x += holder_rod_length
stand_rod3_length = stand_rod_length

stand_rod4_position = copy.copy(holder_rod_position)
stand_rod4_position.x += holder_rod_length
stand_rod4_length = stand_rod_length
holder_rod = cylinder(pos=holder_rod_position, size=vector(holder_rod_length, 0.0, 0.0),
                      radius=0.015, texture=textures.metal, color=color.black)
stand_rod1 = cylinder(pos=stand_rod1_position, axis=vector(0.0, 0.0, stand_rod1_length), radius=0.015,
                      texture=textures.metal, color=color.red)
stand_rod2 = cylinder(pos=stand_rod2_position, axis=vector(0.0, 0.0, stand_rod2_length), radius=0.015,
                      texture=textures.metal, color=color.red)
stand_rod3 = cylinder(pos=stand_rod3_position, axis=vector(0.0, 0.0, stand_rod3_length), radius=0.015,
                      texture=textures.metal, color=color.red)
stand_rod4 = cylinder(pos=stand_rod4_position, axis=vector(0.0, 0.0, stand_rod4_length), radius=0.015,
                      texture=textures.metal, color=color.red)

# now do the rotations
rot_axis = vector(1.0, 0.0, 0.0)
opening_angle -= np.pi
stand_rod1.rotate(angle=opening_angle, axis=rot_axis)
stand_rod2.rotate(angle=-opening_angle, axis=rot_axis)
stand_rod3.rotate(angle=opening_angle, axis=rot_axis)
stand_rod4.rotate(angle=-opening_angle, axis=rot_axis)



