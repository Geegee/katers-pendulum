from vpython import *
import numpy as np
import copy
import ruamel.yaml as yaml

from classes import PhysCylinder, PhysPendulum, CoordinateSystem
from objects import big_cyl, small_cyl, rod
from light_barrier import crossed_light_barrier, LightBarrier, StopWatch
from pendulum_simulation import evolve_pendulum

# get the config
with open('config.yaml') as f:
    katers_conf = yaml.safe_load(f)

# scene settings
# TODO rewrite to automatically set attributes
scene.autoscale = katers_conf['scene']['autoscale']
scene.background = getattr(color, katers_conf['scene']['background'])
scene.width = katers_conf['scene']['width']
scene.height = katers_conf['scene']['height']

scene.camera.axis = vector(*katers_conf['scene']['camera']['axis'])
[scene.camera.rotate(angle=angle, axis=vector(*axis)) for angle, axis in katers_conf['scene']['camera']['rotations']]
scene.center = vector(*katers_conf['scene']['center'])

lights = list(local_light(pos=vector(*li), color=getattr(color, co)) for li, co in katers_conf['scene']['lights'])

# some default parameters
run = False
swap = False

def runbutton(b):
    global run
    if run: b.text = 'Run'
    else: b.text = 'Pause'
    run = not run

run_btn = button(text='Run', bind=runbutton)
# physics
gfactor = katers_conf['physics']['gfactor']
time = katers_conf['physics']['time']
dt = katers_conf['physics']['dt']

# if initially the center of the small weight lies inside the rotation axis.
phi_start = katers_conf['physics']['phi_start']
phi = phi_start
phid = katers_conf['physics']['phid']

my_pendulum = PhysPendulum(phi_start, phid, [big_cyl, small_cyl, rod],
                           vector(1.0, 0.0, 0.0), small_cyl.com, dt=dt)

# from which side are we passing the light barrier?
my_lightbarrier = LightBarrier()
my_stopwatch = StopWatch()


light_barrier_pos = False
light_barrier_neg = False

stop_clock = 0.0

# ideally one could move the mass directly with the mouse along the axis.
# This function is equivalent to moving the big mass up / down by using the
# thread on the rod. 
def move_big_cyl(length):
    # update the objects
    # which mass is moveable under what condition?
    # Only big mass can be moved
    dir_vec = -vec(0, -np.sin(my_pendulum.phi), np.cos(my_pendulum.phi))
    my_pendulum.parts[0].pos = dir_vec * (length.value - my_pendulum.parts[0].axis.mag / 2)
    my_pendulum.parts[0].calc_com()
    my_pendulum.parts[0].calc_moi()
    # update the physical parameters
    my_pendulum.calc_comb_com()
    my_pendulum.calc_total_moi()
    wt.text = '{:1.2f}'.format(length.value)
    return

# Turn around the physical pendulum
def swap_cyl():
    # update the objects
    dir_vec = vec(0, -np.sin(my_pendulum.phi), np.cos(my_pendulum.phi))
    new_small_cyl_pos = copy.copy(my_pendulum.parts[0].com) + dir_vec * my_pendulum.parts[1].axis.mag / 2
    my_pendulum.parts[0].pos = copy.copy(my_pendulum.parts[1].com) + dir_vec * my_pendulum.parts[0].axis.mag / 2
    my_pendulum.parts[1].pos = new_small_cyl_pos
    my_pendulum.dphi = 0.0
    my_pendulum.ang_mom = 0.0
    # update the physical parameters
    my_pendulum.parts[0].calc_com()
    my_pendulum.parts[0].calc_moi()
    my_pendulum.parts[1].calc_com()
    my_pendulum.parts[1].calc_moi()
    my_pendulum.calc_comb_com()
    my_pendulum.calc_total_moi()
    print(my_pendulum.moi)
    return

def swapbutton(b):
    global swap
    if swap:
        b.text = 'Big mass up'
        sl.disabled = False
    else:
        b.text = 'Small mass up'
        sl.disabled = True
    swap_cyl()
    swap = not swap

def set_angle(phi):
    global phi_start
    diphi = my_pendulum.phi - phi.value
    my_pendulum.phi -= diphi
    my_pendulum.dphi = 0.0
    my_pendulum.ang_mom = 0.0
    my_pendulum.rotate(-diphi)
    phi_start = my_pendulum.phi
    wt_phi.text = '{:1.2f}'.format(my_pendulum.phi)

measured_period = False
def measure_period():
    global measured_period
    measured_period = True

button(text='Swap', bind=swapbutton)
button(text='Take period', bind=measure_period)
initial_value = (my_pendulum.parts[0].pos.mag + my_pendulum.parts[0].axis.mag / 2)

sl = slider(min=0.001, max=initial_value, value=initial_value, length=220, bind=move_big_cyl, right=15)
wt = wtext(text='{:1.2f}'.format(sl.value))
sl_phi = slider(min=0.0, max= 2 * np.pi, value=phi_start, length=220, bind=set_angle)
wt_phi = wtext(text='{:1.2f}'.format(sl_phi.value))
period_graph = graph(height=200, title='Period', fast=False,
      xtitle= '<i>time t (s)</i>, s', ytitle='<i>measured period T (s)</i>m')
phase_graph = graph(height=200, title='Phase', fast=False,
      xtitle= '<i>time t (s)</i>, s', ytitle='<i>phase (rad)</i>')
measurement_graph = graph(height=200, title='Measure Period', fast=False,
      xtitle= '<i>length l (m)</i>, s', ytitle='<i>period T (s)</i>')
measured_period_curve = gcurve(color=color.red, graph=period_graph)
phasen_true_curve = gcurve(color=color.red, graph=phase_graph)
phasen_curve = gcurve(color=color.blue, graph=phase_graph, size=6, label='dots')

big_mass_down_true_dots = gdots(color=color.yellow, graph=measurement_graph, size=6, label='Big mass down (approx)')
big_mass_up_true_dots = gdots(color=color.green, graph=measurement_graph, size=6, label='Big mass up (approx)')
big_mass_down_dots = gdots(color=color.blue, graph=measurement_graph, size=6, label='Big mass down')
big_mass_up_dots = gdots(color=color.red, graph=measurement_graph, size=6, label='Big mass up')
expected_period = 2 * np.pi * np.sqrt(my_pendulum.moi / (my_pendulum.total_mass * my_pendulum.com.mag *
                                                         gfactor))

print("tm / com / gfactor / phi: {} {} {} {}".format(my_pendulum.total_mass, my_pendulum.com,
                                                     gfactor, phi))
while True:
    rate(1 / dt)
    if run:
        my_pendulum = evolve_pendulum(my_pendulum, dt)
        phasen_curve.plot(time, my_pendulum.phi)
        phasen_true_curve.plot(time, phi_start * np.cos(2 * np.pi * time / expected_period))
        my_pendulum.rotate(my_pendulum.dphi)
        expected_period = 2 * np.pi * np.sqrt(my_pendulum.moi / (my_pendulum.total_mass *
                                                                 my_pendulum.com.mag * gfactor))
        time += dt
        
        if crossed_light_barrier(my_pendulum):
            my_lightbarrier.triggered()

        if my_lightbarrier.get_counter() % 2 == 0:
            my_stopwatch.start_timer(time)
        else:
            period = my_stopwatch.read_watch(time)
            
        if measured_period:
            if swap:
                big_mass_down_dots.plot(my_pendulum.parts[1].com.mag, period ** 2)
                big_mass_down_true_dots.plot(my_pendulum.parts[1].com.mag, expected_period ** 2)
            else:
                big_mass_up_dots.plot(my_pendulum.parts[0].com.mag, period ** 2)
                big_mass_up_true_dots.plot(my_pendulum.parts[0].com.mag, expected_period ** 2)
            measured_period = False
