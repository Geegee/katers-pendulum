# Katers Pendulum
Simulate Katers Pendulum with python and visualize it with vpython.
## Setting up the environment 
Create a virtual environment with 
```
python -m venv vpython-env
```
additional dependencies are __ruamel.yaml__ the __vpython__ package which can 
both be installed via pip (see in Install dependencies). 
## Activate the environment 
### Linux 
In the terminal run 
```
source ./path/to/your/environment/bin/activate
```
## Install dependencies
```
pip install ruamel.yaml vpython
```
## Running 
Just run:
```
python katerspendulum.py
```
The basic functionality is to use a slider to give some initial deflection. 
The pendulum can also be turned around with the swap button.
The first graph records the phase (blue for the small angle approximation, red the exact solution)
The second graph records the period of the pendulum. 
The graphs as well as the data can be exported using the web interface.

<img src="./images/graphs.png"  width="400" height="400">

<img src="./images/pendulum.png"  width="400" height="600">

