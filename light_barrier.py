def crossed_light_barrier(pendulum):
    if (pendulum.phi > 0 and (pendulum.phi + pendulum.dphi) < 0):
        return True
    else:
        return False

class LightBarrier:
    def __init__(self):
        self._passed = 0
        return 

    def triggered(self):
        self._passed += 1

    def get_counter(self):
        return self._passed

    def reset(self):
        self._passed = 0

class StopWatch:
    def __init__(self):
        self._start = False
        self._time = 0.0

    def start_timer(self, time):
        self._time = time

    def read_watch(self, time):
        start_time = self._time
        self._time = time
        return self._time - start_time

    def reset(self):
        self._time = 0.0
